### What is this repository for? ###

* An NGINX plugin for parsing a tile URI into a meta tile for load balancing purposes

### Dependencies ###

* libpcre2

### Usage ###

This plugin creates the directive parse_meta_tile_uri which takes 2 arguments.

* the variable name
* the URI to parse

Example usage:

```
upstream test {
 		parse_meta_tile_uri $meta_tile_key $request_uri;
 		hash $meta_tile_key consistent;
 		server 127.0.0.1:8081;
 		server 127.0.0.1:8082;
 		server 127.0.0.1:8083;
 		server 127.0.0.1:8084;
}
```

This plugin can be used in location and upstream blocks