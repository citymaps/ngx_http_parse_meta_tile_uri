//
// Created by adam on 11/9/17.
//

#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

static const char* tile_parse_regex = "(^/(?P<tileset>[a-z]+)/v(?P<version>\\d+)(?P<lang>/[a-z]{2,6})?/(?P<z>\\d+)/(?P<x>\\d+)/(?P<y>\\d+)$)";
static const int meta_tile_size = 4;

char* ngx_http_parse_meta_tile_uri(ngx_conf_t *, ngx_command_t *, void *);
void *ngx_http_parse_meta_tile_uri_create_conf(ngx_conf_t *);

typedef struct {
    ngx_array_t  *values;
    ngx_array_t  *lengths;
    pcre2_code *regex_code;
} ngx_http_parse_meta_tile_uri_conf_t;

static ngx_command_t ngx_http_parse_meta_tile_uri_commands[] = {
    {
        ngx_string("parse_meta_tile_uri"),
        NGX_HTTP_UPS_CONF | NGX_HTTP_LOC_CONF | NGX_CONF_TAKE2,
        ngx_http_parse_meta_tile_uri,
        NGX_HTTP_LOC_CONF_OFFSET,
        0,
        NULL
    },
    ngx_null_command
};

static ngx_http_module_t  ngx_http_parse_meta_tile_uri_module_ctx = {
        NULL,                          /* preconfiguration */
        NULL,                          /* postconfiguration */

        NULL,                          /* create main configuration */
        NULL,                          /* init main configuration */

        ngx_http_parse_meta_tile_uri_create_conf, /* create server configuration */
        NULL,                          /* merge server configuration */

        ngx_http_parse_meta_tile_uri_create_conf, /* create location configuration */
        NULL                           /* merge location configuration */
};

ngx_module_t  ngx_http_parse_meta_tile_uri_module = {
        NGX_MODULE_V1,
        &ngx_http_parse_meta_tile_uri_module_ctx,           /* module context */
        ngx_http_parse_meta_tile_uri_commands,              /* module directives */
        NGX_HTTP_MODULE,                               /* module type */
        NULL,                                          /* init master */
        NULL,                                          /* init module */
        NULL,                                          /* init process */
        NULL,                                          /* init thread */
        NULL,                                          /* exit thread */
        NULL,                                          /* exit process */
        NULL,                                          /* exit master */
        NGX_MODULE_V1_PADDING
};

ngx_int_t ngx_http_parse_meta_tile_uri_variable(ngx_http_request_t *r, ngx_http_variable_value_t *v, uintptr_t data) {
    ngx_str_t val;
    ngx_http_parse_meta_tile_uri_conf_t *conf = (ngx_http_parse_meta_tile_uri_conf_t *)data;

    if (ngx_http_script_run(r, &val, conf->lengths->elts, 0, conf->values->elts) == NULL) {
        return NGX_ERROR;
    }

    pcre2_match_data *match_data = pcre2_match_data_create_from_pattern(conf->regex_code, NULL);
    int result = pcre2_match(conf->regex_code, val.data, val.len, 0, 0, match_data, NULL);
    if (result < 0) {
        ngx_log_error(NGX_LOG_DEBUG, r->connection->log, 0, "No match found for uri %s", (const char *)val.data);
        // We didn't find a match, so just return the base URI
        v->data = val.data;
        v->len = (uint32_t) val.len;
        v->valid = 1;
        v->no_cacheable = 0;
        v->not_found = 0;
        return NGX_OK;
    }

    PCRE2_UCHAR *langString, *zoomString, *xString, *yString;
    size_t langSize, zoomSize, xSize, ySize;
    pcre2_substring_get_byname(match_data, (PCRE2_SPTR)"lang", &langString, &langSize);
    pcre2_substring_get_byname(match_data, (PCRE2_SPTR)"x", &xString, &xSize);
    pcre2_substring_get_byname(match_data, (PCRE2_SPTR)"y", &yString, &ySize);
    pcre2_substring_get_byname(match_data, (PCRE2_SPTR)"z", &zoomString, &zoomSize);

    int x = atoi((const char *)xString);
    int y = atoi((const char *)yString);
    int zoom = atoi((const char *)zoomString);

    int meta_x = x - (x % meta_tile_size);
    int meta_y = y - (y % meta_tile_size);

    u_char *buf = ngx_palloc(r->pool, 512);
    u_char *end = ngx_snprintf(buf, 512, "%s|%d|%d|%d", (const char *)&langString[1], zoom, meta_x, meta_y);

    v->data = buf;
    v->len = (uint32_t) (end - buf);
    v->valid = 1;
    v->no_cacheable = 0;
    v->not_found = 0;

    pcre2_match_data_free(match_data);

    ngx_log_error(NGX_LOG_DEBUG, r->connection->log, 0, "Meta Tile URI :%s\n", (const char *)buf);

    return NGX_OK;
}


void *ngx_http_parse_meta_tile_uri_create_conf(ngx_conf_t *cf)
{
    ngx_http_parse_meta_tile_uri_conf_t  *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_parse_meta_tile_uri_conf_t));
    if (conf == NULL) {
        return NULL;
    }

    int err;
    size_t err_offset;
    conf->regex_code = pcre2_compile((PCRE2_SPTR)tile_parse_regex, PCRE2_ZERO_TERMINATED, 0, &err, &err_offset, NULL);

    return conf;
}

char* ngx_http_parse_meta_tile_uri(ngx_conf_t *cf, ngx_command_t *cmd, void *conf) {
    ngx_str_t *value;
    ngx_http_variable_t *v;

    ngx_http_script_compile_t sc;
    ngx_http_parse_meta_tile_uri_conf_t *loc_conf;

    loc_conf = conf;

    value = cf->args->elts;

    ngx_memzero(&sc, sizeof(ngx_http_script_compile_t));

    sc.cf = cf;
    sc.source = &value[2];
    sc.lengths = &loc_conf->lengths;
    sc.values = &loc_conf->values;
    sc.variables = ngx_http_script_variables_count(sc.source);
    sc.complete_lengths = 1;
    sc.complete_values = 1;

    if (ngx_http_script_compile(&sc) != NGX_OK) {
        return NGX_CONF_ERROR;
    }

    if (value[1].data[0] != '$')
        return "needs variable as the first argument";

    value[1].data++;
    value[1].len--;

    v = ngx_http_add_variable(cf, &value[1], NGX_HTTP_VAR_CHANGEABLE);

    v->get_handler = ngx_http_parse_meta_tile_uri_variable;
    v->data = (uintptr_t) conf;

    return NGX_CONF_OK;
}

